import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class CSVReader {
    public static void main(String[] args) {
//        String path = "./matches.csv";
        String path = "./deliveries.csv";
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

//            br.readLine();

            while((line = br.readLine()) != null){
//                System.out.println(line);
                String[] values = line.split(",");
                System.out.println(Arrays.toString(values));

            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
