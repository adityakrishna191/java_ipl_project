import java.io.*;
import java.util.*;
// matches played per year
public class Problem1 {
    public static void main(String[] args) {

        System.out.println(resultProblem1());

//        String path = "./matches.csv";
//        String line = "";
//
//        Map<String, Integer> matchesPlayedPerYear = new TreeMap<>();
//
//        try{
//            BufferedReader br = new BufferedReader(new FileReader(path));
//            br.readLine();
//
//            while((line = br.readLine()) != null){
//                String[] lineSplit = line.split(",");
//
//                if(!matchesPlayedPerYear.containsKey(lineSplit[1])){
//                    matchesPlayedPerYear.put(lineSplit[1],0);
//                }
//                else{
//                    int count = matchesPlayedPerYear.get(lineSplit[1]) + 1;
//                    matchesPlayedPerYear.replace(lineSplit[1],count);
//                }
//            }
//
//        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }


//        System.out.println(matchesPlayedPerYear);
    }
    public static Map resultProblem1(){
        String path = "./matches.csv";
        String line = "";

        Map<String, Integer> matchesPlayedPerYear = new TreeMap<>();

        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            br.readLine();

            while((line = br.readLine()) != null){
                String[] lineSplit = line.split(",");

                if(!matchesPlayedPerYear.containsKey(lineSplit[1])){
                    matchesPlayedPerYear.put(lineSplit[1],0);
                }
                else{
                    int count = matchesPlayedPerYear.get(lineSplit[1]) + 1;
                    matchesPlayedPerYear.replace(lineSplit[1],count);
                }
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        System.out.println(matchesPlayedPerYear);
        return matchesPlayedPerYear;
    }
}
