import java.io.*;
import java.util.*;
// economy of bowlers in 2015
public class Problem4 {
    public static void main(String[] args) {

        System.out.println(resultProblem4());

//        String matchPath = "./matches.csv";
//        String matchLine = "";
//
//        List matchesIdIn2015 = new ArrayList();
//
//        try {
//            BufferedReader matchData = new BufferedReader(new FileReader(matchPath));
//            matchData.readLine();
//
//            while ((matchLine = matchData.readLine()) != null){
//                String[] matchDataSplit = matchLine.split(",");
//                String matchId = matchDataSplit[0];
//                String year = matchDataSplit[1];
//
//                if(year.equals("2015")){
//                    matchesIdIn2015.add(matchId);
//                }
//            }
//
//        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//
////        System.out.println(matchesIdIn2015);
//
//        String deliveryPath = "./deliveries.csv";
//        String deliveryLine = "";
//
//        Map<String, TreeMap<String, Integer>> topEconomicalBowlersIn2015 = new TreeMap<>();
//
//        try {
//            BufferedReader deliveryData = new BufferedReader(new FileReader(deliveryPath));
//            deliveryData.readLine();
//
//            while((deliveryLine = deliveryData.readLine()) != null){
//                String[] deliveryDataSplit = deliveryLine.split(",");
//                String deliveryMatchId = deliveryDataSplit[0];
//                String bowlerName = deliveryDataSplit[8];
//                int runsConceded = Integer.parseInt(deliveryDataSplit[17]);
//
//                if(matchesIdIn2015.contains(deliveryMatchId)){
//
//                    if(!topEconomicalBowlersIn2015.containsKey(bowlerName)){
//                        topEconomicalBowlersIn2015.put(bowlerName, new TreeMap<>());
//                        topEconomicalBowlersIn2015.get(bowlerName).put("ballsDelivered", 0);
//                        topEconomicalBowlersIn2015.get(bowlerName).put("runsConceded", 0);
//                    }
//                    else{
//                        int ballsBowled = topEconomicalBowlersIn2015.get(bowlerName).get("ballsDelivered") + 1;
//                        int totalRunsConceded = topEconomicalBowlersIn2015.get(bowlerName).get("runsConceded") + runsConceded;
//
//                        topEconomicalBowlersIn2015.get(bowlerName).replace("ballsDelivered", ballsBowled);
//                        topEconomicalBowlersIn2015.get(bowlerName).replace("runsConceded", totalRunsConceded);
//                    }
//                }
//            }
//
//        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//
////        System.out.println(topEconomicalBowlersIn2015);
//
//        Map<String, Double> topEconomicalBowlers = new HashMap<>();
//
//        topEconomicalBowlersIn2015.forEach((key, value) -> {
//            String bowlerName = key;
//            int ballsBowled = value.get("ballsDelivered");
//            double oversBowled = ballsBowled/6;
//            int runsConceded = value.get("runsConceded");
//            double economyRate = runsConceded/oversBowled;
//            String economyRateStrFormat = String.format("%.2f", economyRate);
//            topEconomicalBowlers.put(bowlerName, Double.parseDouble(economyRateStrFormat));
//        });
//
////        System.out.println(topEconomicalBowlers);
//
//        Set<Map.Entry<String, Double>> entrySetOfTopEconomicalBowlers = topEconomicalBowlers.entrySet();
//
//        List<Map.Entry<String, Double>> listOfTopEconomicalBowlers = new ArrayList<>(entrySetOfTopEconomicalBowlers);
//
//        Collections.sort(listOfTopEconomicalBowlers,(o1,o2) -> o1.getValue().compareTo(o2.getValue()));
//
//        List top10EconomicalBowlers = listOfTopEconomicalBowlers.subList(0, 1);
//
//        System.out.println(top10EconomicalBowlers);

    }

    public static List resultProblem4(){
        String matchPath = "./matches.csv";
        String matchLine = "";

        List matchesIdIn2015 = new ArrayList();

        try {
            BufferedReader matchData = new BufferedReader(new FileReader(matchPath));
            matchData.readLine();

            while ((matchLine = matchData.readLine()) != null){
                String[] matchDataSplit = matchLine.split(",");
                String matchId = matchDataSplit[0];
                String year = matchDataSplit[1];

                if(year.equals("2015")){
                    matchesIdIn2015.add(matchId);
                }
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

//        System.out.println(matchesIdIn2015);

        String deliveryPath = "./deliveries.csv";
        String deliveryLine = "";

        Map<String, TreeMap<String, Integer>> topEconomicalBowlersIn2015 = new TreeMap<>();

        try {
            BufferedReader deliveryData = new BufferedReader(new FileReader(deliveryPath));
            deliveryData.readLine();

            while((deliveryLine = deliveryData.readLine()) != null){
                String[] deliveryDataSplit = deliveryLine.split(",");
                String deliveryMatchId = deliveryDataSplit[0];
                String bowlerName = deliveryDataSplit[8];
                int runsConceded = Integer.parseInt(deliveryDataSplit[17]);

                if(matchesIdIn2015.contains(deliveryMatchId)){

                    if(!topEconomicalBowlersIn2015.containsKey(bowlerName)){
                        topEconomicalBowlersIn2015.put(bowlerName, new TreeMap<>());
                        topEconomicalBowlersIn2015.get(bowlerName).put("ballsDelivered", 0);
                        topEconomicalBowlersIn2015.get(bowlerName).put("runsConceded", 0);
                    }
                    else{
                        int ballsBowled = topEconomicalBowlersIn2015.get(bowlerName).get("ballsDelivered") + 1;
                        int totalRunsConceded = topEconomicalBowlersIn2015.get(bowlerName).get("runsConceded") + runsConceded;

                        topEconomicalBowlersIn2015.get(bowlerName).replace("ballsDelivered", ballsBowled);
                        topEconomicalBowlersIn2015.get(bowlerName).replace("runsConceded", totalRunsConceded);
                    }
                }
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

//        System.out.println(topEconomicalBowlersIn2015);

        Map<String, Double> topEconomicalBowlers = new HashMap<>();

        topEconomicalBowlersIn2015.forEach((key, value) -> {
            String bowlerName = key;
            int ballsBowled = value.get("ballsDelivered");
            double oversBowled = ballsBowled/6;
            int runsConceded = value.get("runsConceded");
            double economyRate = runsConceded/oversBowled;
            String economyRateStrFormat = String.format("%.2f", economyRate);
            topEconomicalBowlers.put(bowlerName, Double.parseDouble(economyRateStrFormat));
        });

//        System.out.println(topEconomicalBowlers);

        Set<Map.Entry<String, Double>> entrySetOfTopEconomicalBowlers = topEconomicalBowlers.entrySet();

        List<Map.Entry<String, Double>> listOfTopEconomicalBowlers = new ArrayList<>(entrySetOfTopEconomicalBowlers);

        Collections.sort(listOfTopEconomicalBowlers,(o1,o2) -> o1.getValue().compareTo(o2.getValue()));

        List top10EconomicalBowlers = listOfTopEconomicalBowlers.subList(0, 1);

//        System.out.println(top10EconomicalBowlers);
        return top10EconomicalBowlers;
    }
}
