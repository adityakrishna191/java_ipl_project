//Display the team name,its players batting and bowling analysis for a particular match(ex:match id=1)?

import java.io.*;
import java.util.*;

public class Problem6 {
    public static void main(String[] args) {
        String deliveryPath = "./deliveries.csv";
        String deliveryLine = "";

        Scanner in = new Scanner(System.in);

        System.out.println("Enter the match id: ");
        int id = in.nextInt();
        if(id > 636){
            System.out.println("No match data with id "+id);

        }
//        break;
//        {
//          team1
//              {
//              batting
//                  { batsman = runs }
//              bowling
//                  { bowler {
//                            runs = runCount, wickets = wicketCount
//                           }
//                  }
//              }
//          team2
//              {
//              batting
//                  { batsman = runs }
//              bowling
//                  { bowler {
//                            runs = runCount, wickets = wicketCount
//                           }
//                  }
//              }
//        }
        Map<String, Map<String, Integer>> teamBatting = new HashMap<>();
        Map<String, Map<String, Map<String, Integer>>> teamBowling = new HashMap<>();

        try {
            BufferedReader deliveryData = new BufferedReader(new FileReader(deliveryPath));
            deliveryData.readLine();

            while((deliveryLine = deliveryData.readLine()) != null){
                String[] deliveryDataSplit = deliveryLine.split(",");
//                System.out.println(deliveryDataSplit);
                int matchId = Integer.parseInt(deliveryDataSplit[0]);
                String battingTeam = deliveryDataSplit[2];
                String bowlingTeam = deliveryDataSplit[3];
                String batsmanName = deliveryDataSplit[6];
                String bowlerName = deliveryDataSplit[8];
                int batsmanRuns = Integer.parseInt(deliveryDataSplit[15]);
                int bowlerRuns = Integer.parseInt(deliveryDataSplit[17]);
//                String wicket = deliveryDataSplit[18];
//                int wicketCount;
//                if(!wicket.equals("")){
//                    wicketCount = 1;
//                }else{
//                    wicketCount = 0;
//                }

                if(matchId == id){
                    //batting
                    if(!(teamBatting.containsKey(battingTeam))){
                        teamBatting.put(battingTeam, new HashMap<>());
                    }
                    if(!(teamBatting.get(battingTeam).containsKey(batsmanName))){
                        teamBatting.get(battingTeam).put(batsmanName, 0);
                    }
                    if(teamBatting.get(battingTeam).containsKey(batsmanName)){
                        int updatedCount = teamBatting.get(battingTeam).get(batsmanName)+batsmanRuns;
                        teamBatting.get(battingTeam).put(batsmanName, updatedCount);
                    }
                    //bowling
                    if(!(teamBowling.containsKey(bowlingTeam))){
                        teamBowling.put(bowlingTeam, new HashMap<>());
                    }
                    if(!(teamBowling.get(bowlingTeam).containsKey(bowlerName))){
                        teamBowling.get(bowlingTeam).put(bowlerName, new HashMap<>());
                    }
                    if(!(teamBowling.get(bowlingTeam).get(bowlerName).containsKey("runsConceded"))){
                        teamBowling.get(bowlingTeam).get(bowlerName).put("runsConceded", bowlerRuns);
                    }
                    if(teamBowling.get(bowlingTeam).get(bowlerName).containsKey("runsConceded")){
                        int runsConceded = teamBowling.get(bowlingTeam).get(bowlerName).get("runsConceded") + bowlerRuns;
                        teamBowling.get(bowlingTeam).get(bowlerName).put("runsConceded", runsConceded);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println(teamBatting);
        System.out.println(teamBowling);

    }
}
