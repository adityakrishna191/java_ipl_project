import java.util.*;
import java.io.*;
//matches won per team per year
public class Problem2 {
    public static void main(String[] args) {

        System.out.println(resultProblem2());

//            String path = "./matches.csv";
//            String line = "";
//
//            Map<String, TreeMap<String, Integer>> matchesWonPerTeamPerYear = new TreeMap<>();
//
//            try{
//                BufferedReader br = new BufferedReader(new FileReader(path));
//                br.readLine();
//
//                while((line = br.readLine()) != null){
//                    String[] lineSplit = line.split(",");
//                    String winningTeam = lineSplit[10];
//                    String year = lineSplit[1];
//
//                    if(winningTeam == ""){
//                        continue;
//                    }
//
//                    if(!matchesWonPerTeamPerYear.containsKey(winningTeam)){
//                        matchesWonPerTeamPerYear.put(winningTeam, new TreeMap<>());
//                    }
//                    else{
//                        if(!(matchesWonPerTeamPerYear.get(winningTeam)).containsKey(year)){
//                            matchesWonPerTeamPerYear.get(winningTeam).put(year, 0);
//                        }
//                        else{
//                            int count = matchesWonPerTeamPerYear.get(winningTeam).get(year) + 1;
//                            matchesWonPerTeamPerYear.get(winningTeam).put(year, count);
//                        }
//                    }
//                }
//
//            } catch (FileNotFoundException e) {
//                throw new RuntimeException(e);
//            } catch (IOException e) {
//                throw new RuntimeException(e);
//            }
//
//            System.out.println(matchesWonPerTeamPerYear);
    }

    public static Map resultProblem2 (){
        String path = "./matches.csv";
        String line = "";

        Map<String, TreeMap<String, Integer>> matchesWonPerTeamPerYear = new TreeMap<>();

        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            br.readLine();

            while((line = br.readLine()) != null){
                String[] lineSplit = line.split(",");
                String winningTeam = lineSplit[10];
                String year = lineSplit[1];

                if(winningTeam == ""){
                    continue;
                }

                if(!matchesWonPerTeamPerYear.containsKey(winningTeam)){
                    matchesWonPerTeamPerYear.put(winningTeam, new TreeMap<>());
                }
                else{
                    if(!(matchesWonPerTeamPerYear.get(winningTeam)).containsKey(year)){
                        matchesWonPerTeamPerYear.get(winningTeam).put(year, 0);
                    }
                    else{
                        int count = matchesWonPerTeamPerYear.get(winningTeam).get(year) + 1;
                        matchesWonPerTeamPerYear.get(winningTeam).put(year, count);
                    }
                }
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

//        System.out.println(matchesWonPerTeamPerYear);
        return matchesWonPerTeamPerYear;
    }
}
