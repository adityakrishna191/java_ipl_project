import java.io.*;
import java.util.*;
// extra runs conceded per team in 2016
public class Problem3 {
    public static void main(String[] args) {

        System.out.println(resultProblem3());

//        String matchPath = "./matches.csv";
//        String matchLine = "";
//
//        List matchesIdIn2016 = new ArrayList();
//
//        try {
//            BufferedReader matchData = new BufferedReader(new FileReader(matchPath));
//            matchData.readLine();
//
//            while((matchLine = matchData.readLine()) != null){
//                String[] matchDataSplit = matchLine.split(",");
//                String matchId = matchDataSplit[0];
//                String year = matchDataSplit[1];
//
//                if(year.equals("2016")){
//                    matchesIdIn2016.add(matchId);
//                }
//            }
//
//        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//
////        System.out.println(matchesIdIn2016);
//
//        String deliveryPath = "./deliveries.csv";
//        String deliveryLine = "";
//
//        Map<String, Integer> extraRunsConcededPerTeamIn2016 = new TreeMap<>();
//
//        try {
//            BufferedReader deliveryData = new BufferedReader(new FileReader(deliveryPath));
//            deliveryData.readLine();
//
//            while((deliveryLine = deliveryData.readLine()) != null){
//                String[] deliveryDataSplit = deliveryLine.split(",");
//                String deliveryMatchId = deliveryDataSplit[0];
//                String bowlingTeam = deliveryDataSplit[3];
//                int extraRuns = Integer.parseInt(deliveryDataSplit[16]);
//
//                if(matchesIdIn2016.contains(deliveryMatchId)){
//
//                    if(!extraRunsConcededPerTeamIn2016.containsKey(bowlingTeam)){
//                        extraRunsConcededPerTeamIn2016.put(bowlingTeam, 0);
//                    }
//                    else{
//                        int extraRunsToAdd = extraRunsConcededPerTeamIn2016.get(bowlingTeam) + extraRuns;
//                        extraRunsConcededPerTeamIn2016.replace(bowlingTeam, extraRunsToAdd);
//                    }
//                }
//            }
//
//        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//
//        System.out.println(extraRunsConcededPerTeamIn2016);
    }

    public static Map resultProblem3(){
        String matchPath = "./matches.csv";
        String matchLine = "";

        List matchesIdIn2016 = new ArrayList();

        try {
            BufferedReader matchData = new BufferedReader(new FileReader(matchPath));
            matchData.readLine();

            while((matchLine = matchData.readLine()) != null){
                String[] matchDataSplit = matchLine.split(",");
                String matchId = matchDataSplit[0];
                String year = matchDataSplit[1];

                if(year.equals("2016")){
                    matchesIdIn2016.add(matchId);
                }
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

//        System.out.println(matchesIdIn2016);

        String deliveryPath = "./deliveries.csv";
        String deliveryLine = "";

        Map<String, Integer> extraRunsConcededPerTeamIn2016 = new TreeMap<>();

        try {
            BufferedReader deliveryData = new BufferedReader(new FileReader(deliveryPath));
            deliveryData.readLine();

            while((deliveryLine = deliveryData.readLine()) != null){
                String[] deliveryDataSplit = deliveryLine.split(",");
                String deliveryMatchId = deliveryDataSplit[0];
                String bowlingTeam = deliveryDataSplit[3];
                int extraRuns = Integer.parseInt(deliveryDataSplit[16]);

                if(matchesIdIn2016.contains(deliveryMatchId)){

                    if(!extraRunsConcededPerTeamIn2016.containsKey(bowlingTeam)){
                        extraRunsConcededPerTeamIn2016.put(bowlingTeam, 0);
                    }
                    else{
                        int extraRunsToAdd = extraRunsConcededPerTeamIn2016.get(bowlingTeam) + extraRuns;
                        extraRunsConcededPerTeamIn2016.replace(bowlingTeam, extraRunsToAdd);
                    }
                }
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

//        System.out.println(extraRunsConcededPerTeamIn2016);
        return extraRunsConcededPerTeamIn2016;
    }
}
