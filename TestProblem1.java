import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestProblem1 {
        @Test
        public void test(){
            Problem1 test = new Problem1();
            String actual = test.resultProblem1().toString();
            String expected = "{2008=57, 2009=56, 2010=59, 2011=72, 2012=73, 2013=75, 2014=59, 2015=58, 2016=59, 2017=58}";
//            System.out.println(actual);
//           assertEquals(expected, actual);
           assertEquals("checking matchesPlayedPerYear", expected, actual);
        }
}
