// Find the number of matches played per venue

import java.io.*;
import java.util.*;

public class Problem5 {
    public static void main(String[] args) {
        String path = "./matches.csv";
        String line = "";

        Map<String, Integer> matchesPlayedPerVenue = new TreeMap<>();

        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            br.readLine();

            while((line = br.readLine()) != null){
                String[] lineSplit = line.split(",");

                if(!matchesPlayedPerVenue.containsKey(lineSplit[14])){
                    matchesPlayedPerVenue.put(lineSplit[14],0);
                }
                else{
                    int count = matchesPlayedPerVenue.get(lineSplit[14]) + 1;
                    matchesPlayedPerVenue.replace(lineSplit[14],count);
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println(matchesPlayedPerVenue);
    }
}
