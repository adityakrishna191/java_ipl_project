import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestProblem2 {
    @Test
    public void test(){
        Problem2 test = new Problem2();
        String actual = test.resultProblem2().toString();
        String expected = "{Chennai Super Kings={2008=7, 2009=7, 2010=8, 2011=10, 2012=9, 2013=11, 2014=9, 2015=9}, Deccan Chargers={2008=0, 2009=8, 2010=7, 2011=5, 2012=3}, Delhi Daredevils={2008=6, 2009=9, 2010=6, 2011=3, 2012=10, 2013=2, 2014=1, 2015=4, 2016=6, 2017=4}, Gujarat Lions={2016=8, 2017=2}, Kings XI Punjab={2008=9, 2009=6, 2010=3, 2011=6, 2012=7, 2013=7, 2014=11, 2015=2, 2016=3, 2017=5}, Kochi Tuskers Kerala={2011=4}, Kolkata Knight Riders={2008=5, 2009=2, 2010=6, 2011=7, 2012=11, 2013=5, 2014=10, 2015=6, 2016=7, 2017=7}, Mumbai Indians={2008=6, 2009=4, 2010=10, 2011=9, 2012=9, 2013=12, 2014=6, 2015=9, 2016=6, 2017=10}, Pune Warriors={2011=2, 2012=3, 2013=3}, Rajasthan Royals={2008=11, 2009=5, 2010=5, 2011=5, 2012=6, 2013=10, 2014=6, 2015=6}, Rising Pune Supergiant={2017=8}, Rising Pune Supergiants={2016=3}, Royal Challengers Bangalore={2008=3, 2009=8, 2010=7, 2011=9, 2012=7, 2013=8, 2014=4, 2015=7, 2016=8, 2017=1}, Sunrisers Hyderabad={2013=9, 2014=5, 2015=6, 2016=10, 2017=6}}";
    //            System.out.println(actual);
        assertEquals(expected, actual);
    }
}
