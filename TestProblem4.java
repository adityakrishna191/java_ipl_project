import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestProblem4 {
    @Test
    public void test(){
        Problem4 test = new Problem4();
        String actual = test.resultProblem4().toString();
        String expected = "[RN ten Doeschate=4.0]";
        //            System.out.println(actual);
        assertEquals(expected, actual);
    }
}
