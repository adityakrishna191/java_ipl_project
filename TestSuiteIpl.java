import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestProblem1.class,
        TestProblem2.class,
        TestProblem3.class,
        TestProblem4.class
})

public class TestSuiteIpl {
}