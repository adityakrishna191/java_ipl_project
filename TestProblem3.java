import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestProblem3 {
    @Test
    public void test(){
        Problem3 test = new Problem3();
        String actual = test.resultProblem3().toString();
        String expected = "{Delhi Daredevils=106, Gujarat Lions=98, Kings XI Punjab=100, Kolkata Knight Riders=122, Mumbai Indians=102, Rising Pune Supergiants=108, Royal Challengers Bangalore=151, Sunrisers Hyderabad=107}";
        //            System.out.println(actual);
        assertEquals(expected, actual);
    }
}
